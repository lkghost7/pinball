﻿using System;
using PinBall.Code.Data.Enums;

namespace PinBall.Code.Data.Extensions
{
    [Serializable]
    public class AspectRatioData
    {
        public DeviceModel model;
        public float aspectRatio;
        public float cameraSize;
        public float cameraPositionY;
    }
}