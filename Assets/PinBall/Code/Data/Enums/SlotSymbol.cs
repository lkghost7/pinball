namespace PinBall.Code.Data.Enums
{
    public enum SlotSymbolAngle
    {
        Bar = 30,
        Cherry = 60,
        Grape = 90,
        Lemon = 120,
        Plum = 150,
        Seven = 180
    }
}