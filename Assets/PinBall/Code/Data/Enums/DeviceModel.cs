﻿using System;

namespace PinBall.Code.Data.Enums
{
    [Serializable]
    public enum DeviceModel
    {
        Iphone,
        IphoneXS,
        IphoneXR,
        Ipad,
        IpadPro,
    }
}