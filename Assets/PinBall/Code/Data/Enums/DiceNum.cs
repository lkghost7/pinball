namespace PinBall.Code.Data.Enums
{
    public enum DiceNum
    {
        One = 1,
        Two,
        Tree,
        Four,
        Five,
        Six
    }
}