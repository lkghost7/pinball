namespace PinBall.Code.Data.Enums
{
    public enum SoundId
    {
        Click,
        CardSound,
        ChipSound,
        FailBallSound,
        RouletteSound,
        SlotSound,
        DiceSound,
        TrampolineSound
    }
}