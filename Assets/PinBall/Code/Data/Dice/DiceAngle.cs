using System;

namespace PinBall.Code.Data.Dice
{
    [Serializable]
    public struct DiceAngle
    {
        public float AngleX;
        public float AngleY;
        public float AngleZ;
    }
}