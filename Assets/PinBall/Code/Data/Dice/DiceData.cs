using System;
using PinBall.Code.Data.Enums;

namespace PinBall.Code.Data.Dice
{
    [Serializable]
    public class DiceData
    {
        public DiceNum DiceNum;
        public DiceAngle DiceAngle;
    }
}