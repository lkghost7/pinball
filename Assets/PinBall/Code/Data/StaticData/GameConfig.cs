using PinBall.Code.Data.Dice;
using PinBall.Code.Data.Enums;
using PinBall.Code.Data.Extensions;
using UnityEngine;

namespace PinBall.Code.Data.StaticData
{
    [CreateAssetMenu(fileName = "Game Config", menuName = "Static Data/Game Config")]
    public class GameConfig : ScriptableObject
    {
        [Header("Data")]
        public DiceData[] DiceData;
        public SlotSymbolAngle[] SlotSymbolAngles;
        
        [Header("Score")]
        public int ScoreCardRoulette = 2500;
        public int ScoreSlotMachine = 250;
        public int[] ChipsScore;
        public int[] RouletteScore;

        [Header("Time and speed")]
        public float TimeDrumRotate = 2;
        public float TimeSlotDrumRotate = 1.4f;
        public float TimeDiceRotate = 1.2f;
        public float SpeedRoulette = 7;
        
        [Header("Aspect Ratio and camera")]
        public AspectRatioData[] devicesData;

        [Header("Default")] 
        public int lifeCount = 3;
    }
}