using System;
using UnityEngine;

namespace PinBall.Code.Data.StaticData.Locations
{
    [Serializable]
    public class Location
    {
        public float PositionX;
        public float PositionY;
        public float PositionZ;

        public Vector3 ToVector3() => new Vector3(PositionX, PositionY, PositionZ);
    } 
}