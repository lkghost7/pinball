using UnityEngine;

namespace PinBall.Code.Data.StaticData.Locations
{
    [CreateAssetMenu(fileName = "Location Data", menuName = "Static Data/Location Data")]
    public class LocationData : ScriptableObject
    {
        [Header("Environment")]
        public Location PositionChips;
        public Location PositionDice;
        public Location PositionEnvironment;
        public Location PositionRoulette;
        public Location PositionTrumplineLeft;
        public Location PositionTrumplineRight;
        
        [Header("Game")]
        public Location PositionBall;
        public Location PositionCardRoulette;
        public Location PositionEndGameTrigger;
        public Location PositionLeftFlipper;
        public Location PositionRightFlipper;
        public Location PositionRoundTrigger;
        public Location PositionSpring;
        
        [Header("Slot machine")]
        public Location PositionSlotMachine;
    }
}