using System;
using PinBall.Code.Data.Enums;
using UnityEngine;

namespace PinBall.Code.Data.StaticData.Sounds
{
    [Serializable]
    public class AudioClipData
    {
        public AudioClip Clip;
        public SoundId Id;
    }
}