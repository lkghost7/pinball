using System;

namespace PinBall.Code.Data.Progress
{
    [Serializable]
    public class Settings
    {
        public float SoundVolume;
        public float MusicVolume;

        public Settings()
        {
            SoundVolume = MusicVolume = 0.5f;
        }
    }
}