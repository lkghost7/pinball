using System;

namespace PinBall.Code.Data.Progress
{
    [Serializable]
    public class PlayerProgress
    {
        public int Record;
        public Settings Settings;
        public int CurrentScore;
        public int Life;

        public PlayerProgress(int record, int countLife, int currentScore)
        {
            Record = record;
            Settings = new Settings();
            Life = countLife;
            CurrentScore = currentScore;
        }
    }
}