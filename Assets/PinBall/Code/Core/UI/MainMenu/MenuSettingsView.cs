using System;
using PinBall.Code.Core.UI.ExitPanel;
using PinBall.Code.Services.Input;
using UnityEngine;
using UnityEngine.UI;

namespace PinBall.Code.Core.UI.MainMenu
{
    public class MenuSettingsView : MonoBehaviour
    {
        public event Action<float> OnSoundVolumeUpdate;
        public event Action<float> OnMusicVolumeUpdate;

        [SerializeField] private Slider _soundSlider;
        [SerializeField] private Slider _musicSlider;
        [SerializeField] private Button _musicButton;
        [SerializeField] private Button _soundButton;
        [SerializeField] private Button _closeButton;

        [SerializeField] private GameObject EnableMusicSprite;
        [SerializeField] private GameObject DisableMusicSprite;
        [SerializeField] private GameObject EnableSoundSprite;
        [SerializeField] private GameObject DisableSoundSprite;

        private IInputService _inputService;

        public void Construct(IInputService inputService,
            float soundVolume, float musicVolume, ExitPanelView exitPanelView)
        {
            _inputService = inputService;
            _soundSlider.value = soundVolume;
            _musicSlider.value = musicVolume;
            
            _musicSlider.onValueChanged.AddListener(SendMusicVolumeUpdate);
            _soundSlider.onValueChanged.AddListener(SendSoundVolumeUpdate);
            _musicButton.onClick.AddListener(PressMusicButton);
            _soundButton.onClick.AddListener(PressSoundButton);
            _closeButton.onClick.AddListener(PressCloseButton);
        }

        public void SwitchActive() => Time.timeScale = gameObject.activeSelf ? Hide() : Show();

        private int Show()
        {
            if (_inputService !=null)
                _inputService.DisableInput();
            
            gameObject.SetActive(true);
            
            CheckValueMusic(_musicSlider.value);
            CheckValueSound(_soundSlider.value);
            return 0;
        }

        private int Hide()     
        {
            if (_inputService !=null)
                _inputService.EnableInput();
            
            gameObject.SetActive(false);
            return 1;
        }
        
        private void SendSoundVolumeUpdate(float volume)
        {
            CheckValueSound(volume);
            OnSoundVolumeUpdate?.Invoke(volume);
        }

        private void SendMusicVolumeUpdate(float volume)
        {
            CheckValueMusic(volume);
            OnMusicVolumeUpdate?.Invoke(volume);
        }

        private void PressMusicButton() => EnableMusic(!EnableMusicSprite.activeSelf);
        private void PressSoundButton() => EnableSound(!EnableSoundSprite.activeSelf);

        private void EnableSound(bool isSoundEnable)
        {
            EnableSoundSprite.SetActive(isSoundEnable);
            DisableSoundSprite.SetActive(!isSoundEnable);

            _soundSlider.value = isSoundEnable ? 0.5f : 0;
            SendSoundVolumeUpdate(_soundSlider.value);
        }
        
        private void EnableMusic(bool isSoundEnable)
        {
            EnableMusicSprite.SetActive(isSoundEnable);
            DisableMusicSprite.SetActive(!isSoundEnable);

            _musicSlider.value = isSoundEnable ? 0.5f : 0;
            SendMusicVolumeUpdate(_musicSlider.value);
        }

        private void CheckValueMusic(float value)
        {
            EnableMusicSprite.SetActive(value != 0);
            DisableMusicSprite.SetActive(value == 0);
        }

        private void CheckValueSound(float value)
        {
            EnableSoundSprite.SetActive(value != 0);
            DisableSoundSprite.SetActive(value == 0);
        }

        private void PressCloseButton() => SwitchActive();

        private void OnDestroy()
        {
            _musicSlider.onValueChanged.RemoveListener(SendMusicVolumeUpdate);
            _soundSlider.onValueChanged.RemoveListener(SendSoundVolumeUpdate);
            _musicButton.onClick.RemoveListener(PressMusicButton);
            _soundButton.onClick.RemoveListener(PressSoundButton);
            _closeButton.onClick.RemoveListener(PressCloseButton);
        }
    }
}