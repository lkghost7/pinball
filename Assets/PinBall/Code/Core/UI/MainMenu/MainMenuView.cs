using System;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Sound;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PinBall.Code.Core.UI.MainMenu
{
    public class MainMenuView : MonoBehaviour
    { 
        public event Action OnPlayClick;
        public event Action OnSettingsClick;
        
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private TextMeshProUGUI _recordText;

        private ISoundService _soundService;

        public void Construct(ISoundService soundService, int record)
        {
            _soundService = soundService;
            _settingsButton.onClick.AddListener(OnSettingsButtonClick);
            _playButton.onClick.AddListener(OnPlayButtonClick);
            ShowLastRecord(record);
        }

        private void ShowLastRecord(int record) => _recordText.text = record.ToString();

        private void OnPlayButtonClick()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnPlayClick?.Invoke();
        }

        private void OnSettingsButtonClick()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnSettingsClick?.Invoke();
        }
        
        private void OnDestroy()
        {
            _settingsButton.onClick.RemoveAllListeners();
            _playButton.onClick.RemoveAllListeners();
        }
    }
}