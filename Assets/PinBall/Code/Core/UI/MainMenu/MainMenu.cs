using System;
using PinBall.Code.Infrastructure.StateMachine;
using PinBall.Code.Infrastructure.StateMachine.States;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.SaveLoad;
using PinBall.Code.Services.Sound;

namespace PinBall.Code.Core.UI.MainMenu

{
    public class MainMenu : IDisposable
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly MainMenuView _menuView;
        private readonly MenuSettingsView _settingsView;
        private readonly IPersistentProgress _playerProgress;
        private readonly ISaveLoad _saveLoadService;
        private readonly ISoundService _soundService;

        public MainMenu(IGameStateMachine stateMachine, MainMenuView menuView, MenuSettingsView settingsView,
            IPersistentProgress playerProgress, ISaveLoad saveLoadService, ISoundService soundService)
        {
            _soundService = soundService;
            _saveLoadService = saveLoadService;
            _playerProgress = playerProgress;
            _stateMachine = stateMachine;
            _settingsView = settingsView;
            _menuView = menuView;
            SubscribeMenuComponents();
        }

        public void Dispose()
        {
            _settingsView.OnMusicVolumeUpdate -= UpdateMusicVolume;
            _settingsView.OnSoundVolumeUpdate -= UpdateSoundVolume;
            _menuView.OnPlayClick -= EnterGameplayState;
            _menuView.OnSettingsClick -= _settingsView.SwitchActive;
        }

        private void SubscribeMenuComponents()
        {
            _settingsView.OnMusicVolumeUpdate += UpdateMusicVolume;
            _settingsView.OnSoundVolumeUpdate += UpdateSoundVolume;
            _menuView.OnPlayClick += EnterGameplayState;
            _menuView.OnSettingsClick += _settingsView.SwitchActive;
        }

        private void EnterGameplayState() => _stateMachine.Enter<PrepareGameState>();

        private void UpdateSoundVolume(float newVolume)
        {
            _soundService.SetEffectsVolume(newVolume);
            _playerProgress.Progress.Settings.SoundVolume = newVolume;
            _saveLoadService.SaveProgress();
        }

        private void UpdateMusicVolume(float newVolume)
        {
            _soundService.SetBackgroundVolume(newVolume);
            _playerProgress.Progress.Settings.MusicVolume = newVolume;
            _saveLoadService.SaveProgress();
        }
    }
}