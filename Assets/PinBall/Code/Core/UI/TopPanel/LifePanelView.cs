using UnityEngine;

namespace PinBall.Code.Core.UI.TopPanel
{
    public class LifePanelView : MonoBehaviour
    {
        [SerializeField] private GameObject[] _lives;

        public void ShowCurrentLife(int countLife)
        {
            foreach (var life in _lives)
                life.gameObject.SetActive(false);

            for (int i = 0; i < countLife; i++)
                _lives[i].SetActive(true);
        }
    }
}