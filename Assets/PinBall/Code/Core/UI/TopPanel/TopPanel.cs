using System;
using PinBall.Code.Core.UI.ExitPanel;
using PinBall.Code.Core.UI.MainMenu;
using PinBall.Code.Infrastructure.StateMachine;
using PinBall.Code.Infrastructure.StateMachine.States;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.SaveLoad;
using PinBall.Code.Services.Sound;

namespace PinBall.Code.Core.UI.TopPanel
{
    public class TopPanel : IDisposable
    { 
        private readonly IGameStateMachine _stateMachine;
        private readonly TopPanelView _topPanelView;
        private readonly MenuSettingsView _settingsView;
        private readonly ExitPanelView _exitPanelView;
        private readonly IPersistentProgress _playerProgress;
        private readonly ISaveLoad _saveLoadService;
        private readonly ISoundService _soundService;

        public TopPanel(IGameStateMachine stateMachine, TopPanelView topPanelViewView, MenuSettingsView settingsView, ExitPanelView exitPanelView,
            IPersistentProgress playerProgress, ISaveLoad saveLoadService, ISoundService soundService)
        {
            _soundService = soundService;
            _saveLoadService = saveLoadService;
            _playerProgress = playerProgress;
            _stateMachine = stateMachine;
            _settingsView = settingsView;
            _topPanelView = topPanelViewView;
            _exitPanelView = exitPanelView;
            SubscribeMenuComponents();
        }

        public void Dispose()
        {
            _settingsView.OnMusicVolumeUpdate -= UpdateMusicVolume;
            _settingsView.OnSoundVolumeUpdate -= UpdateSoundVolume;
            
            _topPanelView.OnSettingsClick -= _settingsView.SwitchActive;
            _topPanelView.OnExitClick -= MainMenuButtonClick;
        }

        private void SubscribeMenuComponents()
        {
            _settingsView.OnMusicVolumeUpdate += UpdateMusicVolume;
            _settingsView.OnSoundVolumeUpdate += UpdateSoundVolume;

            _topPanelView.OnSettingsClick += _settingsView.SwitchActive;
            _topPanelView.OnExitClick += MainMenuButtonClick;
        }

        private void MainMenuButtonClick() => _stateMachine.Enter<MainMenuState>();

        private void UpdateSoundVolume(float newVolume)
        {
            _soundService.SetEffectsVolume(newVolume);
            _playerProgress.Progress.Settings.SoundVolume = newVolume;
            _saveLoadService.SaveProgress();
        }

        private void UpdateMusicVolume(float newVolume)
        {
            _soundService.SetBackgroundVolume(newVolume);
            _playerProgress.Progress.Settings.MusicVolume = newVolume;
            _saveLoadService.SaveProgress();
        }
    }
}