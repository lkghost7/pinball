using System;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Sound;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PinBall.Code.Core.UI.TopPanel
{ 
    public class TopPanelView : MonoBehaviour
    {  
        public event Action OnSettingsClick;
        public event Action OnExitClick;
        
        [SerializeField] private TextMeshProUGUI _score;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private LifePanelView _lifePanelView;

        private ISoundService _soundService;

        public void Construct(ISoundService soundService)
        {
            _soundService = soundService;
            _settingsButton.onClick.AddListener(OnSettingsButtonClick);
            _exitButton.onClick.AddListener(OnExitButtonClick);
        }

        public void ShowCurrentLife(int countLife) => _lifePanelView.ShowCurrentLife(countLife);
        public void AddScore(int score) => _score.text = score.ToString();

        private void OnSettingsButtonClick()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnSettingsClick?.Invoke();
        }
        
        private void OnExitButtonClick()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnExitClick?.Invoke();
        }
        
        private void OnDestroy()
        {
            _settingsButton.onClick.RemoveListener(OnSettingsButtonClick);
            _exitButton.onClick.RemoveListener(OnExitButtonClick);
        }
    }
}