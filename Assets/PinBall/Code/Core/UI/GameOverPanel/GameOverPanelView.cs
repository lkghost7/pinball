using System;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Sound;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PinBall.Code.Core.UI.GameOverPanel
{
    public class GameOverPanelView : MonoBehaviour
    {
        public event Action OnRestartButtonClick;
        public event Action OnMainMenuButtonClick;
        
        [SerializeField] private  Button RestartButton;
        [SerializeField] private  Button _mainMenuButton;
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private TextMeshProUGUI _recordText;

        private ISoundService _soundService;

        public void Construct(ISoundService soundService)
        {
            _soundService = soundService;
            RestartButton.onClick.AddListener(PressRestartButton);
            _mainMenuButton.onClick.AddListener(PressMainMenuButton);
        }

        public void Show()
        {
            gameObject.SetActive(true);
            Time.timeScale = 0;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            Time.timeScale = 1;
        }
        
        public void ShowScore(int score,int record)
        {
            _scoreText.text = score.ToString();
            _recordText.text = record.ToString();
        }
        
        private void PressMainMenuButton()
        {
            OnMainMenuButtonClick?.Invoke();
            _soundService.PlayEffectSound(SoundId.Click);
        }

        private void PressRestartButton()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnRestartButtonClick?.Invoke();
        }
        
        private void OnDestroy()
        {
            RestartButton.onClick.RemoveListener(PressRestartButton);
            _mainMenuButton.onClick.RemoveListener(PressMainMenuButton);
        }
    }
}
