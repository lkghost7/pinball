using System;
using PinBall.Code.Infrastructure.StateMachine;
using PinBall.Code.Infrastructure.StateMachine.States;

namespace PinBall.Code.Core.UI.GameOverPanel
{
    public class GameOverPanel : IDisposable
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly GameOverPanelView _gameOverPanelView;

        public GameOverPanel(IGameStateMachine stateMachine, GameOverPanelView gameOverPanelView)
        {
            _stateMachine = stateMachine;
            _gameOverPanelView = gameOverPanelView;
            SubscribeButton();
        }

        public void Dispose()
        {
            _gameOverPanelView.OnRestartButtonClick -= RestartButtonClick;
            _gameOverPanelView.OnMainMenuButtonClick -= MainMenuButtonClick;
        }

        private void SubscribeButton()
        {
            _gameOverPanelView.OnRestartButtonClick += RestartButtonClick;
            _gameOverPanelView.OnMainMenuButtonClick += MainMenuButtonClick;
        }

        private void MainMenuButtonClick() => _stateMachine.Enter<MainMenuState>();
        private void RestartButtonClick() => _stateMachine.Enter<PrepareGameState>();
    }
}