using System;
using PinBall.Code.Infrastructure.StateMachine;
using PinBall.Code.Infrastructure.StateMachine.States;
using UnityEngine;

namespace PinBall.Code.Core.UI.ExitPanel
{
    public class ExitPanel : IDisposable
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly ExitPanelView _exitPanelView;

        public ExitPanel(IGameStateMachine stateMachine, ExitPanelView exitPanelView)
        {
            _stateMachine = stateMachine;
            _exitPanelView = exitPanelView;
            SubscribeButton();
        }

        public void Dispose()
        {
            _exitPanelView.OnExitButtonClick -= ExitButtonClick;
            _exitPanelView.OnMainMenuButtonClick -= MainMenuButtonClick;
        }

        private void SubscribeButton()
        {
            _exitPanelView.OnExitButtonClick += ExitButtonClick;
            _exitPanelView.OnMainMenuButtonClick += MainMenuButtonClick;
        }

        private void MainMenuButtonClick() => _stateMachine.Enter<MainMenuState>();
        private void ExitButtonClick() => Application.Quit();
    }
}