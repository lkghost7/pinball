using System;
using PinBall.Code.Core.UI.MainMenu;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Input;
using PinBall.Code.Services.Sound;
using UnityEngine;
using UnityEngine.UI;

namespace PinBall.Code.Core.UI.ExitPanel
{
    public class ExitPanelView : MonoBehaviour
    {
        public event Action OnExitButtonClick;
        public event Action OnMainMenuButtonClick;
        
        [SerializeField] private  Button _exitButton;
        [SerializeField] private  Button _mainMenuButton;
        [SerializeField] private  Button _closeButton;
     
        private ISoundService _soundService;
        private IInputService _inputService;
        private MenuSettingsView _menuSettingsView;

        public void Construct(ISoundService soundService, IInputService inputService, MenuSettingsView menuSettingsView)
        {
            _soundService = soundService;
            _inputService = inputService;
            _menuSettingsView = menuSettingsView;
                
            _exitButton.onClick.AddListener(PressExitButton);
            _mainMenuButton.onClick.AddListener(PressMainMenuButton);
            _closeButton.onClick.AddListener(PressCloseButton);
        }

        private void Show()
        {
            if (_menuSettingsView.gameObject.activeSelf)
                return;

            _inputService.DisableInput();
            gameObject.SetActive(true);
            Time.timeScale = 0;
        }

        private void Hide()
        {
            _inputService.EnableInput();
            gameObject.SetActive(false);
            Time.timeScale = 1;
        }

        private void PressMainMenuButton()
        {
            Hide();
            OnMainMenuButtonClick?.Invoke();
            _soundService.PlayEffectSound(SoundId.Click);
        }
        
        private void PressCloseButton()
        {
            Hide();
            _soundService.PlayEffectSound(SoundId.Click);
        }

        private void PressExitButton()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnExitButtonClick?.Invoke();
        }
        
        private void OnDestroy()
        {
            _exitButton.onClick.RemoveListener(PressExitButton);
            _mainMenuButton.onClick.RemoveListener(PressMainMenuButton);
            _closeButton.onClick.RemoveListener(PressCloseButton);
        }
    }
}