using System;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Dice
{
    public class DiceTrigger : MonoBehaviour
    {
        public event Action OnTouchDice; 

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Ball"))
                OnTouchDice?.Invoke();
        }
    }
}
