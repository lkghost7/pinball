using System;
using DG.Tweening;
using PinBall.Code.Data.Dice;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Sound;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PinBall.Code.Core.GamePlay.Dice
{
    public class DiceView : MonoBehaviour
    {
        public event Action<DiceNum> OnDroppedDice;

        [SerializeField] private Transform _diceCube;
        [SerializeField] private DiceTrigger _diceTrigger;

        private ISoundService _soundService;
        
        private DiceData[] _diceData;
        private float _timeRotateDice;

        public void Initialize(DiceData[] diceData, float timeDiceRotate, ISoundService soundService)
        {
            _soundService = soundService;
            _diceData = diceData;
            _timeRotateDice = timeDiceRotate;
            _diceTrigger.OnTouchDice += TouchDice;
        }

        private void TouchDice() => DiceRotate(_diceData[Random.Range(0, _diceData.Length)]);

        private void DiceRotate(DiceData diceData)
        {
            _soundService.PlayEffectSound(SoundId.DiceSound);
            _diceCube.DOLocalRotate(new Vector3(diceData.DiceAngle.AngleX + 360,
                    diceData.DiceAngle.AngleY + 360,
                    diceData.DiceAngle.AngleZ + 360),
                    _timeRotateDice, RotateMode.FastBeyond360).SetEase(Ease.Linear).
                OnComplete(() => { OnDroppedDice?.Invoke(diceData.DiceNum);
            });
        }

        private void OnDestroy() => _diceTrigger.OnTouchDice -= TouchDice;
    }
}