using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Flippers
{
    public class Flipper : MonoBehaviour
    {
        [SerializeField] private HingeJoint2D _hingeJoint2D;
        
        public void FlipperOn() => _hingeJoint2D.useMotor = true;
        public void FlipperOff() => _hingeJoint2D.useMotor = false;
    } 
}