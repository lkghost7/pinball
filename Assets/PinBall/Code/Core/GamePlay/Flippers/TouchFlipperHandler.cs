﻿using PinBall.Code.Data.Enums;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Flippers
{ 
    public class TouchFlipperHandler
    {
        private readonly Flipper _leftFlipper;
        private readonly Flipper _rightFlipper;

        private int _resolutionWidthHalf;
        private int _resolutionHeightHalf;
        private TouchStatus _leftFlipperRegister;
        private TouchStatus _rightFlipperRegister;
        
        public TouchFlipperHandler(Flipper leftFlipper, Flipper rightFlipper)
        {
            _leftFlipper = leftFlipper;
            _rightFlipper = rightFlipper;
            CheckResolutionScreen();
        }

        public void TouchPressed(Vector2 point, TouchStatus status)
        {
            if (point.y >= _resolutionHeightHalf)
                return;
            
            if (point.x <= _resolutionWidthHalf)
            {
                _leftFlipper.FlipperOn();
                _leftFlipperRegister = status;
            }

            if (!(point.x >= _resolutionWidthHalf)) return;
            _rightFlipper.FlipperOn();
            _rightFlipperRegister = status;
        }

        public void TouchDepressed(TouchStatus status)
        {
            if (_leftFlipperRegister == status)
                _leftFlipper.FlipperOff();

            if (_rightFlipperRegister == status)
                _rightFlipper.FlipperOff();
        }

        private void CheckResolutionScreen()
        {
            _resolutionWidthHalf = Screen.width / 2;
            _resolutionHeightHalf = Screen.height / 2;
        }
    }
}