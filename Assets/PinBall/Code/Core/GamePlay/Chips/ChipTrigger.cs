using System;
using System.Collections;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Chips
{
    public class ChipTrigger : MonoBehaviour
    { 
        public event Action<int> OnTouchChip;

        [SerializeField] private SpriteRenderer _illuminateSprite;
        private int _score;

        public void Initialize(int chipScore) => _score = chipScore;

        private void OnCollisionEnter2D(Collision2D col)
        {
            if (!col.collider.CompareTag("Ball")) return;
            OnTouchChip?.Invoke(_score);
            StartCoroutine(ShowIlluminate());
        }

        private IEnumerator ShowIlluminate()
        {
            _illuminateSprite.enabled = true;
            yield return new WaitForSecondsRealtime(0.5f);
            _illuminateSprite.enabled = false;
        }
    }
}
