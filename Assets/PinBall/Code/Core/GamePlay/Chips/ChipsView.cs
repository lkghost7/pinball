using System;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Sound;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Chips
{
    public class ChipsView : MonoBehaviour
    {
        public event Action<int> OnAddScore;
        private ISoundService _soundService;
        
        [SerializeField] private ChipTrigger[] _chips;
 
        public void Initialize(int[] chipsScore, ISoundService soundService)
        {
            _soundService = soundService;
            
            for (var i = 0; i < _chips.Length; i++)
            {
                ChipTrigger chip = _chips[i];
                chip.Initialize(chipsScore[i]);
                chip.OnTouchChip += TouchChip;
            }
        }

        private void TouchChip(int score)
        {
            _soundService.PlayEffectSound(SoundId.ChipSound);
            OnAddScore?.Invoke(score);
        }

        private void OnDestroy()
        {
            foreach (ChipTrigger chip in _chips)
                chip.OnTouchChip -= TouchChip;
        }
    }
}
