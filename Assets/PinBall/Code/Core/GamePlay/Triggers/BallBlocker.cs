using System;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Triggers
{
    public class BallBlocker : MonoBehaviour
    {
        public event Action OnBallEnterGame;
        
        [SerializeField] private RoundTrigger _roundTrigger;

        private void Start() => _roundTrigger.OnBallEnterGame += BallEnterGame;
        private void BallEnterGame() => OnBallEnterGame?.Invoke();
        private void OnDestroy() => _roundTrigger.OnBallEnterGame -= BallEnterGame;
    }
}
