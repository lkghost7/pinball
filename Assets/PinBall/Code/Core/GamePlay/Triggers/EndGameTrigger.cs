using System;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Triggers
{
    public class EndGameTrigger : MonoBehaviour
    {
        public event Action OnBallFail;
         
        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Ball"))
                OnBallFail?.Invoke();
        }
    }
}