using System;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Triggers
{
    public class RoundTrigger : MonoBehaviour
    {
        public event Action OnBallEnterGame;
         
        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Ball"))
                OnBallEnterGame?.Invoke();
        }
    }
}