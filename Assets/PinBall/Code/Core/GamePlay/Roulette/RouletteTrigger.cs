using System;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Roulette
{
    public class RouletteTrigger : MonoBehaviour
    {
        public event Action<int> OnTouchRouletteSlot;

        private int _score;

        public void Initialize(int score) => _score = score;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Ball"))
                OnTouchRouletteSlot?.Invoke(_score);
        }
    }
}