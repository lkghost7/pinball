using System;
using DG.Tweening;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Sound;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Roulette
{
    public class RouletteView : MonoBehaviour
    {
        public event Action<int> OnAddScore;
        
        [SerializeField] private Transform _iternalRoulette;
        [SerializeField] private RouletteTrigger[] _rouletteTriggers;

        private ISoundService _soundService;
        private float _speedRoulette;
    
        public void Initialize(float speedRoulette, int[] scoreRoulette, ISoundService soundService)
        {
            _soundService = soundService;
            _speedRoulette = speedRoulette;
            RouletteStartRotate();

            for (var i = 0; i < _rouletteTriggers.Length; i++)
            {
                RouletteTrigger rouletteTrigger = _rouletteTriggers[i];
                rouletteTrigger.Initialize(scoreRoulette[i]);
                rouletteTrigger.OnTouchRouletteSlot += TouchRouletteSlot;
            }
        }

        private void TouchRouletteSlot(int score)
        {
            _soundService.PlayEffectSound(SoundId.RouletteSound);
            OnAddScore?.Invoke(score);
        }

        private void RouletteStartRotate()
        {
            _iternalRoulette.transform.DOLocalRotate(new Vector3(0, 0, 360), _speedRoulette, RotateMode.FastBeyond360)
                .SetEase(Ease.Linear).SetLoops(-1);
        }
    }
}
