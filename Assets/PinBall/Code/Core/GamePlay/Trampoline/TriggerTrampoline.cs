using System;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Trampoline
{
    public class TriggerTrampoline : MonoBehaviour
    {
        public event Action<Rigidbody2D> OnBallPush;
    
        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Ball"))
                OnBallPush?.Invoke(col.GetComponent<Rigidbody2D>());
        }
    }
}
