using System.Collections;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Sound;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Trampoline
{
    public class TrampolineView : MonoBehaviour
    {
        [SerializeField] private GameObject _spriteActive;
        [SerializeField] private GameObject _spritePassed;
        [SerializeField] private TriggerTrampoline triggerTrampoline;
        [SerializeField] private bool _isRight;

        private ISoundService _soundService;

        private int _pushForceX;
        private int _pushForceY;

        public void Initialize(ISoundService soundService)
        {
            _soundService = soundService;
            triggerTrampoline.OnBallPush += PushBall;
            SetForce();
        }

        private IEnumerator  TrampolineIlluminate()
        {
            _spriteActive.SetActive(false);
            _spritePassed.SetActive(true);
            yield return new WaitForSecondsRealtime(0.2f);
            _spriteActive.SetActive(true);
            _spritePassed.SetActive(false);
        }

        private void SetForce()
        {
            _pushForceY = Random.Range(30, 50);
            _pushForceX = _isRight ? Random.Range(-25,- 15) : Random.Range(15, 25);
        }

        private void PushBall(Rigidbody2D rb)
        {
            _soundService.PlayEffectSound(SoundId.TrampolineSound);
            rb.AddForce(new Vector2(_pushForceX, _pushForceY), ForceMode2D.Impulse);
            StartCoroutine(TrampolineIlluminate());
        }

        private void OnDestroy() => triggerTrampoline.OnBallPush -= PushBall;
    }
}