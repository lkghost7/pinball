using System;
using System.Collections;
using DG.Tweening;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Sound;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.CardRoulette
{
    public class CardView : MonoBehaviour
    {
        public event Action OnCompleteRotate;

        [SerializeField] private CardTrigger _cardTrigger;
        private  float _timeRotate;
        private bool _isFaceCard;
        
        private ISoundService _soundService;
 
        public void Initialize(float timeDrumRotate, ISoundService soundService)
        {
            _soundService = soundService;
            _timeRotate = timeDrumRotate;
            _cardTrigger.OnCardRotate += CardRotate;
            _cardTrigger.EnableTrigger(true);
        }

        private void CardRotate()
        {
            _soundService.PlayEffectSound(SoundId.CardSound);
            _cardTrigger.EnableTrigger(false);
            int rangeAngle = _isFaceCard ? SetRangeAngle(1260, false) : SetRangeAngle(1080, true);
            StartCoroutine(DelayEnableTrigger(_timeRotate / 2));
            transform.DOLocalRotate(new Vector3(rangeAngle, 0, 0), _timeRotate, RotateMode.FastBeyond360)
                .SetEase(Ease.OutQuart).onComplete = () => { OnCompleteRotate?.Invoke(); };
        }

        private int SetRangeAngle(int rangeAngle, bool isFace)
        {
            _isFaceCard = isFace;
            return rangeAngle;
        }

        private IEnumerator DelayEnableTrigger(float delayTime)
        {
            yield return new WaitForSeconds(delayTime);
            _cardTrigger.EnableTrigger(true);
        }

        private void OnDestroy() => _cardTrigger.OnCardRotate -= CardRotate;
    }
}