using System;
using PinBall.Code.Services.Sound;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.CardRoulette
{
    public class CardRouletteView : MonoBehaviour
    {
        public event Action<int> OnAddScore;

        [SerializeField] private CardView[] _cardViews;
        private int _score;
        
        public void Construct(int scoreCardRoulette, float timeDrumRotate,ISoundService soundService)
        {
            _score = scoreCardRoulette;
            foreach (CardView cardView in _cardViews)
                cardView.Initialize(timeDrumRotate, soundService);

            SubScribeCards();
        }

        private void CheckWin()
        {
            int countWin = 0;
            foreach (CardView card in _cardViews)
            {
                card.transform.rotation.ToAngleAxis(out var angleX, out _);
                if (angleX == 360 || angleX == 0)
                    countWin++;
            }

            if (countWin == _cardViews.Length)
                OnAddScore?.Invoke(_score);
        }

        private void SubScribeCards()
        {
            foreach (CardView card in _cardViews)
                card.OnCompleteRotate += CheckWin;
        }

        private void UnSubScribeCards()
        {
            foreach (CardView card in _cardViews)
                card.OnCompleteRotate -= CheckWin;
        }

        private void OnDestroy() => UnSubScribeCards();
    }
}