using System;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.CardRoulette
{
    public class CardTrigger : MonoBehaviour
    {
        public event Action OnCardRotate;
        private bool _isEnableTrigger;

        public void EnableTrigger(bool enable) => _isEnableTrigger = enable;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!_isEnableTrigger)
                return;

            if (col.CompareTag("Ball"))
                OnCardRotate?.Invoke();
        }
    }
}