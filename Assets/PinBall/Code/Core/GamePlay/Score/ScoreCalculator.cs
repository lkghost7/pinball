using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Factories.UIFactory;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.SaveLoad;

namespace PinBall.Code.Core.GamePlay.Score
{
    public class ScoreCalculator
    {
        private readonly IPersistentProgress _persistentProgress;
        private readonly ISaveLoad _saveLoad;
        private readonly IUIFactory _uiFactory;

        private int _currentScore;
        private int _lastScore;
        private int _lifeCount;
        
        public ScoreCalculator(IPersistentProgress persistentProgress, ISaveLoad saveLoad,
            IUIFactory uiFactory)
        {
            _persistentProgress = persistentProgress;
            _saveLoad = saveLoad;
            _uiFactory = uiFactory;
        }
        
        public int GetCountLife() => _lifeCount;
        public int DecrementLife() => _lifeCount--;
        
        public void AddScore(int score)
        {
            _lastScore = score;
            _currentScore += score;
            _uiFactory.TopPanelView.AddScore(_currentScore);
        }
        
        public void LoadCurrentProgressValue()
        {
            _saveLoad.LoadProgress();
            _lifeCount = _persistentProgress.Progress.Life;
            _currentScore = _persistentProgress.Progress.CurrentScore;
        }
        
        public void AddDiceScore(DiceNum diceNum)
        {
            int score = (int)diceNum * _lastScore;
            AddScore(score);
            _lastScore = 0;
        }

        public void SaveRecord()
        {
            if (_persistentProgress.Progress.Record >= _currentScore)
                return;

            _persistentProgress.Progress.Record = _currentScore;
            _saveLoad.SaveProgress();
        }
        
        public void SaveCurrentProgressValue()
        {
            _persistentProgress.Progress.CurrentScore = _currentScore;
            _persistentProgress.Progress.Life = _lifeCount;
            _saveLoad.SaveProgress();
        }
    }
}