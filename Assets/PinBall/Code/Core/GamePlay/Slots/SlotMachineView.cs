using System;
using System.Collections;
using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Sound;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PinBall.Code.Core.GamePlay.Slots
{
    public class SlotMachineView : MonoBehaviour
    {
        public event Action<int> OnAddScore;
 
        [SerializeField] private DrumView[] _drums;
        [SerializeField] private LeverTriggers[] _leverTriggers;
        [SerializeField] private GameObject[] _illuminates;

        private readonly SlotSymbolAngle[] _slotsLoseSymbolAngle = {0, 0, 0};
        private SlotSymbolAngle[] _slotSymbolAngle;
        private SlotSymbolAngle _slotWinnerSymbolAngle;
        private float _timeSlotDrumRotate;
        private int _scoreWinSlotMachine;
        private Coroutine _illuminateCoroutine;
        
        private ISoundService _soundService;

        public void Initialize(SlotSymbolAngle[] slotSymbolAngles, float timeSlotDrumRotate, int scoreSlot, ISoundService soundService)
        {
            _soundService = soundService;
            _slotSymbolAngle = slotSymbolAngles;
            _timeSlotDrumRotate = timeSlotDrumRotate;
            _scoreWinSlotMachine = scoreSlot;
            foreach (LeverTriggers leverTrigger in _leverTriggers)
                leverTrigger.OnEnterLever += EnterLever;

            StartIlluminate();
        }

        public void StartIlluminate() => _illuminateCoroutine ??= StartCoroutine(ShowIlluminate());

        public void StopIlluminate()
        {
            if (_illuminateCoroutine == null) return;
            StopCoroutine(_illuminateCoroutine);
            _illuminateCoroutine = null;
        }
        
        private void RotateDrums(int isWinCombination, float timeRotate)
        {
            int score = isWinCombination == 0 ? SetScoreWinner(timeRotate): SetScoreLose(timeRotate);
            StartCoroutine(DelayRotateRoulette(score));
        }

        private int SetScoreLose(float timeRotate)
        {
            GetRandomLose();
            RotateDrumLoseCombination(timeRotate);
            int score = 0;
            return score;
        }

        private int SetScoreWinner(float timeRotate)
        {
            GetRandomWinner();
            RotateDrumWinCombination(timeRotate);
            int score = _scoreWinSlotMachine;
            return score;
        }

        private IEnumerator ShowIlluminate()
        {
            while (true)
            {
                for (int i = 0; i < _illuminates.Length; i++)
                {
                    foreach (GameObject illuminate in _illuminates)
                        illuminate.SetActive(false);

                    _illuminates[i].SetActive(true);
                    yield return new WaitForSeconds(1);
                }
            }
        }

        private void EnterLever()
        {
            _soundService.PlayEffectSound(SoundId.SlotSound);
            int isWinStatus = Random.Range(0, 4);
            RotateDrums(isWinStatus, _timeSlotDrumRotate);

            foreach (LeverTriggers leverTrigger in _leverTriggers)
                leverTrigger.LeverDown();
        }

        private IEnumerator DelayRotateRoulette(int score)
        {
            yield return new WaitForSecondsRealtime(_timeSlotDrumRotate);
            foreach (LeverTriggers leverTrigger in _leverTriggers)
            {
                leverTrigger.LeverUp();
            }

            OnAddScore?.Invoke(score);
        }
        
        private void GetRandomWinner() => _slotWinnerSymbolAngle = _slotSymbolAngle[Random.Range(0, _slotSymbolAngle.Length)];

        private void GetRandomLose()
        {
            _slotsLoseSymbolAngle[0] = _slotSymbolAngle[Random.Range(0, _slotSymbolAngle.Length)];
            _slotsLoseSymbolAngle[1] = _slotSymbolAngle[Random.Range(0, 3)];
            _slotsLoseSymbolAngle[2] = _slotSymbolAngle[Random.Range(3, _slotSymbolAngle.Length)];
        }

        private void RotateDrumLoseCombination(float timeRotate)
        {
            for (int i = 0; i < _drums.Length; i++)
                _drums[i].DrumRotate(_slotsLoseSymbolAngle[i], timeRotate);
        }

        private void RotateDrumWinCombination(float timeRotate)
        {
            foreach (DrumView drum in _drums)
                drum.DrumRotate(_slotWinnerSymbolAngle, timeRotate);
        }

        private void OnDestroy()
        {
            foreach (LeverTriggers leverTrigger in _leverTriggers)
                leverTrigger.OnEnterLever -= EnterLever;
        }
    }
}