using System;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Slots
{
    public class LeverTriggers : MonoBehaviour
    {
        public event Action OnEnterLever;
        
        [SerializeField] private GameObject _upLever;
        [SerializeField] private GameObject _downLever;

        private bool _isLeverDown;

        public void LeverUp()
        {
            _isLeverDown = false;
            _upLever.SetActive(true);
            _downLever.SetActive(false);
        }

        public void LeverDown()
        {
            _isLeverDown = true;
            _upLever.SetActive(false);
            _downLever.SetActive(true);
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (_isLeverDown)
                return;

            if (col.CompareTag("Ball"))
                OnEnterLever?.Invoke();
        }
    }
}