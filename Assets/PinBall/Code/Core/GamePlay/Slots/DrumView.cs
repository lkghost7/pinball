using DG.Tweening;
using PinBall.Code.Data.Enums;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PinBall.Code.Core.GamePlay.Slots
{
    public class DrumView : MonoBehaviour
    {
        [SerializeField] private Transform _cylinder;

        public void DrumRotate(SlotSymbolAngle angle, float timeRotate)
        {
            int currentAngle = (int)angle;
            int circles = 360 * Random.Range(2, 7);
            currentAngle += circles;

            _cylinder.DOLocalRotate(new Vector3(0, 0, currentAngle), timeRotate,
                RotateMode.FastBeyond360).SetEase(Ease.OutQuart);
        }
    }
}