using System;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Spring
{
    public class PushBallTrigger : MonoBehaviour
    {
        public event Action<Rigidbody2D> OnPushBall;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Ball"))
                OnPushBall?.Invoke(col.GetComponent<Rigidbody2D>());
        }
    }
}