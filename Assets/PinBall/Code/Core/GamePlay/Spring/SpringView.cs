using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Spring
{
    public class SpringView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _springSprite;
        [SerializeField] private SpriteRenderer _HeadbandSprite;
        [SerializeField] private PushBallTrigger _pushBallTrigger;
        [SerializeField] private AnimationCurve _ballForceCurve;

        private float _currentScaleY;
        private float _forcePushY;
        private float _forcePushX;
        private bool _isPushSpring;
        private bool _isEnableSpring;

        public void Initialize() => _pushBallTrigger.OnPushBall += PushBall;
        public void EnableSpring(bool isEnable) => _isEnableSpring = isEnable;
        public void PressSpring() => _isPushSpring = true;
        public void UnPressSpring() => _isPushSpring = false;

        private void Update()
        {
            if (!_isEnableSpring)
                return;
            
            if (_isPushSpring)
            {
                CompressSpring();
                SetForce();
                return;
            }

            if (_currentScaleY >= 0.99f)
            {
                _forcePushY = 0;
                return;
            }

            SpringBack();
        }

        private void SpringBack()
        {
            var localScale = _springSprite.transform.localScale;
            localScale = Vector3.Lerp(localScale, new Vector3(1f, 1f, 1f), 0.3f);
            _springSprite.transform.localScale = localScale;
            _HeadbandSprite.transform.localPosition = Vector3.Lerp(_HeadbandSprite.transform.localPosition,
                new Vector3(0f, 0f, 0f), 0.3f);
            _currentScaleY = localScale.y;
        }

        private void CompressSpring()
        {
            Vector3 localScale = _springSprite.transform.localScale;
            localScale = Vector3.Lerp(localScale, new Vector3(1f, 0.3f, 1f), 0.02f);
            _springSprite.transform.localScale = localScale;
            _HeadbandSprite.transform.localPosition = Vector3.Lerp(_HeadbandSprite.transform.localPosition,
                new Vector3(0f, -0.6f, 0f), 0.02f);
            _currentScaleY = localScale.y;
        }

        private void SetForce()
        {
            float value = Mathf.Abs(_HeadbandSprite.transform.localPosition.y);
            _forcePushY = _ballForceCurve.Evaluate(value);
            _forcePushX = Random.Range(-15, 15);
        }

        private void PushBall(Rigidbody2D rb) =>
            rb.AddForce(new Vector2(_forcePushX, _forcePushY), ForceMode2D.Impulse);

        private void OnDestroy() => _pushBallTrigger.OnPushBall -= PushBall;
    }
}