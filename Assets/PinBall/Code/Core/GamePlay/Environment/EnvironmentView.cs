using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Environment
{
    public class EnvironmentView : MonoBehaviour
    {
        [SerializeField] private ArrowView _arrowViewField;
        [SerializeField] private ArrowView _arrowViewPush;

        public void Initialize()
        {
            _arrowViewField.StartIlluminate();
            _arrowViewPush.StartIlluminate();
        }
    }
}
