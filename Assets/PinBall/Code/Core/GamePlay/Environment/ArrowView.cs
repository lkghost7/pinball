using System.Collections;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Environment
{
    public class ArrowView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _greenArrow;
        [SerializeField] private SpriteRenderer _yellowArrow;
    
        private Coroutine _illuminateCoroutine;
        
        public void StartIlluminate() => _illuminateCoroutine ??= StartCoroutine(ShowIlluminate());

        public void StopIlluminate()
        {
            if (_illuminateCoroutine == null) return;
            StopCoroutine(_illuminateCoroutine);
            _illuminateCoroutine = null;
        }
    
        private IEnumerator ShowIlluminate()
        {
            while (true)
            {
                _greenArrow.gameObject.SetActive(false);
                _yellowArrow.gameObject.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                _greenArrow.gameObject.SetActive(true);
                _yellowArrow.gameObject.SetActive(false);
                yield return new WaitForSeconds(0.5f);
            }
        }
    }
}
