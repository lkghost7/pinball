﻿using System;
using System.Collections.Generic;
using PinBall.Code.Data.Extensions;
using UnityEngine;

namespace PinBall.Code.Core.GamePlay.Environment
{
    public class AspectRatioChanger
    {
        private readonly Camera _camera;
        private readonly Dictionary<float, AspectRatioData> _aspectRatioData = new Dictionary<float, AspectRatioData>();

        public AspectRatioChanger(AspectRatioData[] aspectRatioData)
        {
            _camera = Camera.main;
            foreach (AspectRatioData ratioData in aspectRatioData)
                _aspectRatioData.Add(ratioData.aspectRatio, ratioData);

            if (_aspectRatioData.ContainsKey(GetAspectRatio()))
                SetGameSceneForAspectRatio(GetAspectRatio());
        }

        private float GetAspectRatio()
        {
            float resolution = (float) Screen.width / Screen.height;
            return (float) Math.Round(resolution, 1);
        }

        private void SetGameSceneForAspectRatio(float aspectRatioResolution)
        {
            AspectRatioData aspectRatioData = _aspectRatioData[aspectRatioResolution];
            _camera.orthographicSize = aspectRatioData.cameraSize;
            Vector3 camPos = _camera.transform.position;
            _camera.transform.position = new Vector3(camPos.x, aspectRatioData.cameraPositionY, camPos.z);
        }
    }
}