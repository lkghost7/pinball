using System;
using System.Collections.Generic;
using PinBall.Code.Infrastructure.StateMachine.States;
using PinBall.Code.Services.Factories.GameFactory;
using PinBall.Code.Services.Factories.UIFactory;
using PinBall.Code.Services.Input;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.SaveLoad;
using PinBall.Code.Services.SceneLoader;
using PinBall.Code.Services.Sound;
using PinBall.Code.Services.StaticData;

namespace PinBall.Code.Infrastructure.StateMachine
{
    public class GameStateMachine : IGameStateMachine
    {
        private readonly Dictionary<Type, IExitableState> _states;
        private IExitableState _activeState;

        public GameStateMachine(ServiceContainer.ServiceContainer container, ISoundService soundService,
            ICoroutineRunner coroutineRunner)
        {
            _states = new Dictionary<Type, IExitableState>()
            {
                [typeof(BootstrapState)] = new BootstrapState(this, container, soundService),
                [typeof(LoadProgressState)] = new LoadProgressState(this, container.Single<IPersistentProgress>(), container.Single<ISaveLoad>(), container.Single<IStaticData>(), soundService),
                [typeof(MainMenuState)] = new MainMenuState(this, container.Single<ISceneLoader>(), container.Single<IUIFactory>()),
                [typeof(PinBallGameState)] = new PinBallGameState(this,  container.Single<IInputService>(), container.Single<IGameFactory>(), container.Single<IUIFactory>(), container.Single<IEnvironmentFactory>(), container.Single<ISoundService>()),
                [typeof(CalculateState)] = new CalculateState(  container.Single<IUIFactory>(), container.Single<IPersistentProgress>(), container.Single<ISaveLoad>()),
                [typeof(PrepareGameState)] = new PrepareGameState(this, container.Single<ISceneLoader>(), container.Single<IUIFactory>(), container.Single<IGameFactory>(), container.Single<IPersistentProgress>(), container.Single<ISaveLoad>(), container.Single<IEnvironmentFactory>()),
                [typeof(LaunchingBallState)] = new LaunchingBallState(this,  container.Single<IGameFactory>(), container.Single<IInputService>(), container.Single<IEnvironmentFactory>(), container.Single<ISoundService>()),
            };
        }

        public void Enter<TState>() where TState : class, IState =>
            ChangeState<TState>().Enter();

        public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload> =>
            ChangeState<TState>().Enter(payload);

        private TState GetState<TState>() where TState : class, IExitableState
            => _states[typeof(TState)] as TState;

        private TState ChangeState<TState>() where TState : class, IExitableState
        {
            _activeState?.Exit();
            TState state = GetState<TState>();
            _activeState = state;
            return state;
        }
        
        ~GameStateMachine() => _activeState.Exit();
    }
}