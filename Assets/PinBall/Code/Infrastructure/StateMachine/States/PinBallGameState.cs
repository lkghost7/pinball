using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Factories.GameFactory;
using PinBall.Code.Services.Factories.UIFactory;
using PinBall.Code.Services.Input;
using PinBall.Code.Services.Sound;

namespace PinBall.Code.Infrastructure.StateMachine.States
{
    public class PinBallGameState : IState
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly IInputService _inputService;
        private readonly IGameFactory _gameFactory;
        private readonly IEnvironmentFactory _environmentFactory;
        private readonly IUIFactory _uiFactory;
        private readonly ISoundService _soundService;

        public PinBallGameState(IGameStateMachine stateMachine,
            IInputService inputService, IGameFactory gameFactory, IUIFactory uiFactory,
            IEnvironmentFactory environmentFactory, ISoundService soundService)
        {
            _stateMachine = stateMachine;
            _inputService = inputService;
            _gameFactory = gameFactory;
            _uiFactory = uiFactory;
            _environmentFactory = environmentFactory;
            _soundService = soundService;
        }
  
        public void Enter()
        {
            _environmentFactory.ScoreCalculator.LoadCurrentProgressValue();
            SubscribeTouchControl();
            SubscribeAddScore();
            
            _environmentFactory.EndGameTrigger.OnBallFail += BallFail;
        }
        public void Exit()
        {
            UnSubscribeTouchControl();
            UnSubscribeAddScore();
            _environmentFactory.EndGameTrigger.OnBallFail -= BallFail;
        }

        private void SubscribeTouchControl()
        {
            _inputService.EnableInput();
            _inputService.OnTouchPressed += _gameFactory.TouchFlipperHandler.TouchPressed;
            _inputService.OnTouchDepressed += _gameFactory.TouchFlipperHandler.TouchDepressed;
        } 
        
        private void UnSubscribeTouchControl()
        {
            _inputService.DisableInput();
            _inputService.OnTouchPressed -= _gameFactory.TouchFlipperHandler.TouchPressed;
            _inputService.OnTouchDepressed -= _gameFactory.TouchFlipperHandler.TouchDepressed;
        }

        private void SubscribeAddScore()
        {
            _gameFactory.CardRouletteView.OnAddScore += _environmentFactory.ScoreCalculator.AddScore;
            _gameFactory.RouletteView.OnAddScore += _environmentFactory.ScoreCalculator.AddScore;
            _gameFactory.ChipsView.OnAddScore += _environmentFactory.ScoreCalculator.AddScore;
            _gameFactory.SlotMachineView.OnAddScore += _environmentFactory.ScoreCalculator.AddScore;
            _gameFactory.DiceView.OnDroppedDice += _environmentFactory.ScoreCalculator.AddDiceScore;
        }
        
        private void UnSubscribeAddScore()
        {
            _gameFactory.CardRouletteView.OnAddScore -= _environmentFactory.ScoreCalculator.AddScore;
            _gameFactory.RouletteView.OnAddScore -= _environmentFactory.ScoreCalculator.AddScore;
            _gameFactory.ChipsView.OnAddScore -= _environmentFactory.ScoreCalculator.AddScore;
            _gameFactory.SlotMachineView.OnAddScore -= _environmentFactory.ScoreCalculator.AddScore;
            _gameFactory.DiceView.OnDroppedDice -= _environmentFactory.ScoreCalculator.AddDiceScore;
        }

        private void BallFail()
        {
            _soundService.PlayEffectSound(SoundId.FailBallSound);
            _environmentFactory.ScoreCalculator.DecrementLife();
            _environmentFactory.ScoreCalculator.SaveCurrentProgressValue();
            
            if (_environmentFactory.ScoreCalculator.GetCountLife() != 0)
            {
                _uiFactory.TopPanelView.ShowCurrentLife(_environmentFactory.ScoreCalculator.GetCountLife());
                _stateMachine.Enter<LaunchingBallState>();
            }
            else
            {
                _uiFactory.TopPanelView.ShowCurrentLife(_environmentFactory.ScoreCalculator.GetCountLife());
                _environmentFactory.ScoreCalculator.SaveRecord();
                _stateMachine.Enter<CalculateState>();
            }
        }
    }
}