using PinBall.Code.Data.Enums;
using PinBall.Code.Services.Factories.GameFactory;
using PinBall.Code.Services.Input;
using PinBall.Code.Services.Sound;
using UnityEngine;

namespace PinBall.Code.Infrastructure.StateMachine.States
{
    public class LaunchingBallState : IState
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly IGameFactory _gameFactory;
        private readonly IEnvironmentFactory _environmentFactory;
        private readonly IInputService _inputService;
        private readonly ISoundService _soundService;
        private int _resolutionHeightHalf;

        public LaunchingBallState(IGameStateMachine stateMachine, 
            IGameFactory gameFactory, IInputService inputService,
            IEnvironmentFactory environmentFactory, ISoundService soundService)
        {
            _stateMachine = stateMachine;
            _gameFactory = gameFactory;
            _inputService = inputService;
            _environmentFactory = environmentFactory;
            _soundService = soundService;
        }
 
        public void Enter()
        {
            _inputService.EnableInput();
            _inputService.OnTouchPressed += TouchPressed;
            _inputService.OnTouchDepressed += TouchDepressed;
            _environmentFactory.BallBlocker.OnBallEnterGame += RoundTriggerEnter;
            _gameFactory.SpringView.EnableSpring(true);
            CheckResolutionScreen();
            SetBallPosition();
        }

        public void Exit()
        {
            _inputService.DisableInput();
            _inputService.OnTouchPressed -= TouchPressed;
            _inputService.OnTouchDepressed -= TouchDepressed;
            _environmentFactory.BallBlocker.OnBallEnterGame -= RoundTriggerEnter;
            _gameFactory.SpringView.EnableSpring(false);
        }

        private void SetBallPosition()
        {
            Vector3 targetPosition = _gameFactory.SpringView.gameObject.transform.position;
            _gameFactory.Ball.transform.position = new Vector3(targetPosition.x, targetPosition.y + 2.4f, -1);
        }

        private void TouchPressed(Vector2 point, TouchStatus status)
        {
            if (point.y >= _resolutionHeightHalf)
                return;
            
            _soundService.EnableSpringEffectBall();
            _gameFactory.SpringView.PressSpring();
        }

        private void TouchDepressed(TouchStatus status)
        {
            _soundService.DisableSpringEffectBall();
            _gameFactory.SpringView.UnPressSpring();
        }
        
        private void CheckResolutionScreen() => _resolutionHeightHalf = Screen.height / 2;
        private void RoundTriggerEnter() => _stateMachine.Enter<PinBallGameState>();
    }
}