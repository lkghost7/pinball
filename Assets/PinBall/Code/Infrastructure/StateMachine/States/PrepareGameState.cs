using PinBall.Code.Services.Factories.GameFactory;
using PinBall.Code.Services.Factories.UIFactory;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.SaveLoad;
using PinBall.Code.Services.SceneLoader;
using UnityEngine;

namespace PinBall.Code.Infrastructure.StateMachine.States
{
    public class PrepareGameState : IState
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly ISceneLoader _sceneLoader;
        private readonly IUIFactory _uiFactory;
        private readonly IGameFactory _gameFactory;
        private readonly IEnvironmentFactory _environmentFactory;
        private readonly IPersistentProgress _playerProgress;
        private readonly ISaveLoad _saveLoadService;

        private const string PinBallSceneName = "PinBallScene";

        public PrepareGameState(IGameStateMachine stateMachine, ISceneLoader sceneLoader,
            IUIFactory uiFactory, IGameFactory gameFactory, IPersistentProgress persistentProgress,
            ISaveLoad saveLoad, IEnvironmentFactory environmentFactory)
        { 
            _uiFactory = uiFactory;
            _sceneLoader = sceneLoader;
            _stateMachine = stateMachine;
            _gameFactory = gameFactory;
            _playerProgress = persistentProgress;
            _saveLoadService = saveLoad;
            _environmentFactory = environmentFactory;
        }

        public void Enter()
        {
            InitializeStartGameValue();
            _sceneLoader.LoadScene(PinBallSceneName, CreateGamePlayUI);
        }

        public void Exit() { }
        
        private void InitializeStartGameValue()
        {
            _playerProgress.Progress.Life = 3;
            _playerProgress.Progress.CurrentScore = 0;
            _saveLoadService.SaveProgress();
        }

        private void CreateGamePlayUI()
        {
            _uiFactory.ActiveScene = PinBallSceneName;
            Transform uIRoot = _uiFactory.CreateRootCanvas().transform;
            _uiFactory.CreateTopPanel(uIRoot);
            _uiFactory.CreateGameOverPanel(uIRoot);
            CreateGamePlayElement();
            _stateMachine.Enter<LaunchingBallState>();
        }

        private void CreateGamePlayElement()
        {
            _gameFactory.CreateSpring();
            _gameFactory.CreateBall();
            _environmentFactory.CreateBallBlocker();
            _environmentFactory.CreateEndGameTrigger();

            _gameFactory.CreateFlippers();
            _gameFactory.CreateCardRoulette();
            _environmentFactory.CreateTrampolines();

            _gameFactory.CreateRouletteView();
            _gameFactory.CreateChipsView();
            _environmentFactory.CreateEnvironment();
            _gameFactory.CreateDiceView();
            _gameFactory.CreateSlotMachineView();
            
            _gameFactory.CreateTouchControl();
            _environmentFactory.CreateScoreCalculator();
            _environmentFactory.CreateAspectRatioChanger();
        }
    }
}