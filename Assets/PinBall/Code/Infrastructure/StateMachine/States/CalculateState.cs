using PinBall.Code.Services.Factories.UIFactory;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.SaveLoad;

namespace PinBall.Code.Infrastructure.StateMachine.States
{
    public class CalculateState : IState
    {
        private readonly IUIFactory _uiFactory;
        private readonly IPersistentProgress _persistentProgress;
        private readonly ISaveLoad _saveLoad;

        public CalculateState(IUIFactory uiFactory, IPersistentProgress persistentProgress, ISaveLoad saveLoad)
        {
            _uiFactory = uiFactory;
            _persistentProgress = persistentProgress;
            _saveLoad = saveLoad;
        }

        public void Exit() => _uiFactory.GameOverPanelView.Hide();

        public void Enter()
        {
            _saveLoad.LoadProgress();
            int currentScore = _persistentProgress.Progress.CurrentScore;
            int record = _persistentProgress.Progress.Record;
            _uiFactory.GameOverPanelView.Show();
            _uiFactory.GameOverPanelView.ShowScore(currentScore, record);
        }
    }
}