using PinBall.Code.Data.Progress;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.SaveLoad;
using PinBall.Code.Services.Sound;
using PinBall.Code.Services.StaticData;

namespace PinBall.Code.Infrastructure.StateMachine.States
{
    public class LoadProgressState : IState
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly IPersistentProgress _playerProgress;
        private readonly ISaveLoad _saveLoadService;
        private readonly IStaticData _staticDataService;
        private readonly ISoundService _soundService;

        public LoadProgressState(IGameStateMachine stateMachine, IPersistentProgress playerProgress,
            ISaveLoad saveLoadService, IStaticData staticDataService, ISoundService soundService)
        {
            _soundService = soundService;
            _staticDataService = staticDataService;
            _saveLoadService = saveLoadService;
            _playerProgress = playerProgress;
            _stateMachine = stateMachine;
        }
         
        public void Enter()
        {
            LoadProgressOrInitNew();
            InitializeSoundVolume();
            _stateMachine.Enter<MainMenuState>();
        }

        public void Exit() { }
        
        private void LoadProgressOrInitNew() =>
            _playerProgress.Progress = _saveLoadService.LoadProgress() ?? CreateNewProgress();

        private PlayerProgress CreateNewProgress() => new PlayerProgress(0, _staticDataService.GameConfig.lifeCount, 0);

        private void InitializeSoundVolume()
        {
            PlayerProgress progress = _playerProgress.Progress;
            _soundService.SetBackgroundVolume(progress.Settings.MusicVolume);
            _soundService.SetEffectsVolume(progress.Settings.SoundVolume);
            _soundService.EnableBackgroundMusic();
        }
    }
}