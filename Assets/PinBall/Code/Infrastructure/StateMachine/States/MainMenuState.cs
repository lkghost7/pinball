using PinBall.Code.Services.Factories.UIFactory;
using PinBall.Code.Services.SceneLoader;

namespace PinBall.Code.Infrastructure.StateMachine.States
{
    public class MainMenuState : IState
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly ISceneLoader _sceneLoader;
        private readonly IUIFactory _uiFactory;

        private const string MainMenuSceneName = "MainMenu";

        public MainMenuState(IGameStateMachine stateMachine, ISceneLoader sceneLoader, 
            IUIFactory uiFactory)
        {
            _uiFactory = uiFactory;
            _sceneLoader = sceneLoader;
            _stateMachine = stateMachine;
        }
        
        public void Enter() => _sceneLoader.LoadScene(MainMenuSceneName, CreateMenuUI);
        public void Exit() { }
 
        private void CreateMenuUI()
        {
            _uiFactory.ActiveScene = MainMenuSceneName;
            _uiFactory.CreateMainMenu();
        }
    }
}