using PinBall.Code.Services.Dispose;
using PinBall.Code.Services.Factories.GameFactory;
using PinBall.Code.Services.Factories.UIFactory;
using PinBall.Code.Services.Input;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.Providers.AssetProvider;
using PinBall.Code.Services.Providers.StaticDataProvider;
using PinBall.Code.Services.SaveLoad;
using PinBall.Code.Services.SceneLoader;
using PinBall.Code.Services.Sound;
using PinBall.Code.Services.StaticData;

namespace PinBall.Code.Infrastructure.StateMachine.States
{
    public class BootstrapState : IState
    {
        private readonly IGameStateMachine _gameStateMachine;

        public BootstrapState(IGameStateMachine gameStateMachine, ServiceContainer.ServiceContainer container,
            ISoundService soundService)
        {
            _gameStateMachine = gameStateMachine;
            RegisterServices(container, soundService);
        }

        public void Enter() => _gameStateMachine.Enter<LoadProgressState>();

        public void Exit() { }

        private void RegisterServices(ServiceContainer.ServiceContainer container, ISoundService soundService)
        {
            container.RegisterSingle<IGameStateMachine>(_gameStateMachine);
            container.RegisterSingle<IDisposeService>(new DisposeService());
            container.RegisterSingle<IInputService>(new InputService());
            container.RegisterSingle<IPersistentProgress>(new PersistentPlayerProgress());
            container.RegisterSingle<IAssetProvider>(new AssetProvider());
            container.RegisterSingle<IStaticDataProvider>(new StaticDataProvider());
            container.RegisterSingle<ISceneLoader>(new SceneLoader(container.Single<IDisposeService>()));
            RegisterStaticData(container);
            container.RegisterSingle<ISaveLoad>(new PrefsSaveLoad(container.Single<IPersistentProgress>()));

            RegisterSoundService(container, soundService);
            RegisterUIFactory(container);
            RegisterGameFactory(container);
            RegisterEnvironmentFactory(container);
        }

        private void RegisterEnvironmentFactory(ServiceContainer.ServiceContainer container)
        {
            container.RegisterSingle<IEnvironmentFactory>(new EnvironmentFactory(
                container.Single<IDisposeService>(),
                container.Single<IAssetProvider>(),
                container.Single<IStaticData>(),
                container.Single<IPersistentProgress>(),
                container.Single<ISoundService>(),
                container.Single<ISaveLoad>(),
                container.Single<IUIFactory>(),
                container.Single<IGameStateMachine>()));
        }

        private void RegisterGameFactory(ServiceContainer.ServiceContainer container)
        {
            container.RegisterSingle<IGameFactory>(new GameFactory(
                container.Single<IDisposeService>(),
                container.Single<IAssetProvider>(),
                container.Single<IStaticData>(),
                container.Single<IPersistentProgress>(),
                container.Single<ISoundService>(),
                container.Single<ISaveLoad>(),
                container.Single<IUIFactory>(),
                container.Single<IGameStateMachine>()));
        }

        private void RegisterUIFactory(ServiceContainer.ServiceContainer container)
        {
            container.RegisterSingle<IUIFactory>(new UIFactory(
                _gameStateMachine,
                container.Single<IAssetProvider>(),
                container.Single<IStaticData>(),
                container.Single<IPersistentProgress>(),
                container.Single<ISaveLoad>(),
                container.Single<IDisposeService>(),
                container.Single<ISoundService>(),
                container.Single<IInputService>()));
        }

        private void RegisterStaticData(ServiceContainer.ServiceContainer container)
        {
            IStaticData staticData = new StaticData(container.Single<IStaticDataProvider>());
            staticData.LoadStaticData();
            container.RegisterSingle<IStaticData>(staticData);
        }

        private void RegisterSoundService(ServiceContainer.ServiceContainer container, ISoundService soundService)
        {
            soundService.Construct(container.Single<IStaticData>().SoundData);
            container.RegisterSingle<ISoundService>(soundService);
        }
    }
}