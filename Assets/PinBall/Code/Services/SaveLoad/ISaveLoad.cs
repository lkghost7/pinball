using PinBall.Code.Data.Progress;
using PinBall.Code.Infrastructure.ServiceContainer;

namespace PinBall.Code.Services.SaveLoad
{
    public interface ISaveLoad : IService
    {
        void SaveProgress();
        PlayerProgress LoadProgress();
    }
}