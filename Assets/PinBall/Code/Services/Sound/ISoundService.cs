using PinBall.Code.Data.Enums;
using PinBall.Code.Data.StaticData.Sounds;
using PinBall.Code.Infrastructure.ServiceContainer;

namespace PinBall.Code.Services.Sound
{
    public interface ISoundService : IService
    {
        void Construct(SoundData soundData);
        void PlayEffectSound(SoundId soundId);
        void SetBackgroundVolume(float volume);
        void SetEffectsVolume(float volume);
        void EnableBackgroundMusic();
        void DisableBackgroundMusic();
        void EnableSpringEffectBall();
        void DisableSpringEffectBall();
    }
}