using PinBall.Code.Core.UI.ExitPanel;
using PinBall.Code.Core.UI.GameOverPanel;
using PinBall.Code.Core.UI.MainMenu;
using PinBall.Code.Core.UI.TopPanel;
using PinBall.Code.Infrastructure.ServiceContainer;
using UnityEngine;

namespace PinBall.Code.Services.Factories.UIFactory
{
    public interface IUIFactory : IService
    {
        public TopPanelView TopPanelView { get; }
        public GameOverPanelView GameOverPanelView { get; }
        public ExitPanelView ExitPanelView { get; }
        public MenuSettingsView MenuSettingsView { get; }
         
        GameObject CreateRootCanvas();
        MainMenu CreateMainMenu();
        TopPanel CreateTopPanel(Transform uIRoot);
        GameOverPanel CreateGameOverPanel(Transform uIRoot);
        MenuSettingsView CreateMenuSettings(Transform mainMenuTransform);
        string ActiveScene { get; set; }
    }
}