using PinBall.Code.Core.UI.ExitPanel;
using PinBall.Code.Core.UI.GameOverPanel;
using PinBall.Code.Core.UI.MainMenu;
using PinBall.Code.Core.UI.TopPanel;
using PinBall.Code.Data.Progress;
using PinBall.Code.Infrastructure.StateMachine;
using PinBall.Code.Services.Dispose;
using PinBall.Code.Services.Input;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.Providers.AssetProvider;
using PinBall.Code.Services.SaveLoad;
using PinBall.Code.Services.Sound;
using PinBall.Code.Services.StaticData;
using UnityEngine;

namespace PinBall.Code.Services.Factories.UIFactory
{
    public class UIFactory : IUIFactory
    {
        public string ActiveScene { get; set; }

        private readonly IAssetProvider _assetProvider;
        private readonly IGameStateMachine _stateMachine;
        private readonly IPersistentProgress _playerProgress;
        private readonly IStaticData _staticData;
        private readonly ISaveLoad _saveLoadService;
        private readonly IDisposeService _disposeService;
        private readonly ISoundService _soundService;
        private readonly IInputService _inputService;

        public UIFactory(IGameStateMachine stateMachine, IAssetProvider assetProvider, IStaticData staticData,
            IPersistentProgress playerProgress, ISaveLoad saveLoadService,
            IDisposeService disposeService, ISoundService soundService, IInputService inputService)
        {
            _soundService = soundService;
            _saveLoadService = saveLoadService;
            _disposeService = disposeService;
            _playerProgress = playerProgress;
            _staticData = staticData;
            _stateMachine = stateMachine;
            _assetProvider = assetProvider;
            _inputService = inputService;
        }

        public TopPanelView TopPanelView { get; private set; }
        public GameOverPanelView GameOverPanelView { get; private set; }
        public ExitPanelView ExitPanelView { get; private set; }
        public MenuSettingsView MenuSettingsView { get; private set; }

        public GameObject CreateRootCanvas() =>
            Object.Instantiate(_assetProvider.GetRootGameplayUIPrefab());

        public MainMenu CreateMainMenu()
        {
            GameObject mainMenuObject = Object.Instantiate(_assetProvider.GetMainMenuPrefab());
            MenuSettingsView menuSettingsView = CreateMenuSettings(mainMenuObject.transform);
            Settings playerSettings = _playerProgress.Progress.Settings;
            menuSettingsView.Construct(_inputService, playerSettings.SoundVolume, playerSettings.MusicVolume, null);
         
            MainMenuView mainMenuView = CreateMainMenuView(mainMenuObject);
            MainMenu mainMenu = new MainMenu(_stateMachine, mainMenuView, menuSettingsView, _playerProgress,
                _saveLoadService, _soundService);
            _disposeService.RegisterDisposable(mainMenu, ActiveScene);
            return mainMenu;
        }
        
        public TopPanel CreateTopPanel(Transform uIRoot)
        {
            TopPanelView topPanelView = CreateTopPanelView(uIRoot);
            MenuSettingsView menuSettingsView = CreateMenuSettings(uIRoot);
            ExitPanelView exitPanelView = CreateExitPanelView(uIRoot);
            Settings playerSettings = _playerProgress.Progress.Settings;
            menuSettingsView.Construct(_inputService, playerSettings.SoundVolume, playerSettings.MusicVolume, exitPanelView);
            exitPanelView.Construct(_soundService, _inputService, menuSettingsView);
            CreateExitPanel(exitPanelView);

            TopPanel topPanel = new TopPanel(_stateMachine, topPanelView, menuSettingsView,exitPanelView, _playerProgress,
                _saveLoadService, _soundService);
            _disposeService.RegisterDisposable(topPanel, ActiveScene);
            return topPanel;
        }
        
        public GameOverPanel CreateGameOverPanel(Transform uIRoot)
        {
            GameOverPanelView gameOverPanelView = CreateGameOverPanelView(uIRoot);
            GameOverPanel gameOverPanel = new GameOverPanel(_stateMachine, gameOverPanelView);
            _disposeService.RegisterDisposable(gameOverPanel, ActiveScene);
            return gameOverPanel;
        }

        public MenuSettingsView CreateMenuSettings(Transform parentTransform)
        {
            GameObject settingsPanel = Object.Instantiate(_assetProvider.GetSettingsPanelPrefab(), parentTransform);
            MenuSettingsView menuSettingsView = settingsPanel.GetComponent<MenuSettingsView>();
            MenuSettingsView = menuSettingsView;
            return menuSettingsView;
        }
        private ExitPanel CreateExitPanel(ExitPanelView exitPanelView)
        {
            ExitPanel exitPanel = new ExitPanel(_stateMachine, exitPanelView);
            _disposeService.RegisterDisposable(exitPanel, ActiveScene);
            return exitPanel;
        }
        
        private TopPanelView CreateTopPanelView(Transform uiRoot)
        {
            GameObject gameObject = Object.Instantiate(_assetProvider.GetTopPanelView(), uiRoot);
            TopPanelView topPanelView = gameObject.GetComponent<TopPanelView>();
            topPanelView.Construct(_soundService);
            TopPanelView = topPanelView;
            return topPanelView;
        }
        
        private ExitPanelView CreateExitPanelView(Transform uiRoot)
        {
            GameObject gameObject = Object.Instantiate(_assetProvider.GetExitPanelView(), uiRoot);
            ExitPanelView exitPanelView = gameObject.GetComponent<ExitPanelView>();

            ExitPanelView = exitPanelView;
            return exitPanelView;
        }
        
        private GameOverPanelView CreateGameOverPanelView(Transform uiRoot)
        {
            GameObject gameObject = Object.Instantiate(_assetProvider.GetGameOverPanelView(), uiRoot);
            GameOverPanelView gameOverPanelView = gameObject.GetComponent<GameOverPanelView>();
            gameOverPanelView.Construct(_soundService);
            GameOverPanelView = gameOverPanelView;
            return gameOverPanelView;
        }

        private MainMenuView CreateMainMenuView(GameObject mainMenuObject)
        {
            MainMenuView mainMenuView = mainMenuObject.GetComponent<MainMenuView>();
            _saveLoadService.LoadProgress();
            int record = _playerProgress.Progress.Record;
            mainMenuView.Construct(_soundService, record);
            return mainMenuView;
        } 
    }
}