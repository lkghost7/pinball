using PinBall.Code.Core.GamePlay.CardRoulette;
using PinBall.Code.Core.GamePlay.Chips;
using PinBall.Code.Core.GamePlay.Dice;
using PinBall.Code.Core.GamePlay.Flippers;
using PinBall.Code.Core.GamePlay.Roulette;
using PinBall.Code.Core.GamePlay.Slots;
using PinBall.Code.Core.GamePlay.Spring;
using PinBall.Code.Infrastructure.StateMachine;
using PinBall.Code.Services.Dispose;
using PinBall.Code.Services.Factories.UIFactory;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.Providers.AssetProvider;
using PinBall.Code.Services.SaveLoad;
using PinBall.Code.Services.Sound;
using PinBall.Code.Services.StaticData;
using UnityEngine;

namespace PinBall.Code.Services.Factories.GameFactory
{
    public class GameFactory : IGameFactory
    {
        public string ActiveScene { get; set; }
        public SpringView SpringView { get; private set; }
        public GameObject Ball { get; private set; }
        
        public Flipper LeftFlipper { get; private set; }
        public Flipper RightFlipper { get; private set; }
        public CardRouletteView CardRouletteView { get; private set; }
        public RouletteView RouletteView { get; private set; }
        public ChipsView ChipsView { get; private set; }
        public DiceView DiceView { get; private set; }
        public SlotMachineView SlotMachineView { get; private set; }
        public TouchFlipperHandler TouchFlipperHandler { get; private set; }
        
        private readonly IDisposeService _disposeService;
        private readonly IAssetProvider _assetProvider;
        private readonly IStaticData _staticData;
        private readonly IPersistentProgress _playerProgress;
        private readonly ISoundService _soundService;
        private readonly ISaveLoad _saveLoad;
        private readonly IUIFactory _uiFactory;
        private readonly IGameStateMachine _stateMachine;

        public GameFactory(IDisposeService disposeService, IAssetProvider assetProvider,
            IStaticData staticData, IPersistentProgress playerProgress,
            ISoundService soundService, ISaveLoad saveLoad, IUIFactory uiFactory, IGameStateMachine stateMachine)
        {
            _soundService = soundService;
            _playerProgress = playerProgress;
            _staticData = staticData;
            _assetProvider = assetProvider;
            _disposeService = disposeService;
            _saveLoad = saveLoad;
            _uiFactory = uiFactory;
            _stateMachine = stateMachine;
        }
        
        public SpringView CreateSpring()
        {
            GameObject springPrefab = _assetProvider.GetSpringPrefab();
            Vector3 position = _staticData.LocationData.PositionSpring.ToVector3();
            SpringView springView = Object.Instantiate(springPrefab, position, Quaternion.identity).GetComponent<SpringView>();
            springView.Initialize();
            SpringView = springView;
            return SpringView;
        }
        
        public GameObject CreateBall()
        {
            GameObject ballPrefab = _assetProvider.GetBallPrefab();
            Vector3 position = _staticData.LocationData.PositionBall.ToVector3();
            Ball = Object.Instantiate(ballPrefab, position, Quaternion.identity);
            return Ball;
        }
        
        public CardRouletteView CreateCardRoulette()
        {
            GameObject cardRoulettePrefab = _assetProvider.GetCardRoulettePrefab();
            Vector3 position = _staticData.LocationData.PositionCardRoulette.ToVector3();
            CardRouletteView cardRouletteView = Object.Instantiate(cardRoulettePrefab, position, Quaternion.identity).GetComponent<CardRouletteView>();
            cardRouletteView.Construct(_staticData.GameConfig.ScoreCardRoulette, _staticData.GameConfig.TimeDrumRotate, _soundService);
            CardRouletteView = cardRouletteView;
            return CardRouletteView;
        }

        public RouletteView CreateRouletteView()
        {
            GameObject roulettePrefab = _assetProvider.GetRoulettePrefab();
            RouletteView rouletteView = Object.Instantiate(roulettePrefab).GetComponent<RouletteView>();
            rouletteView.Initialize(_staticData.GameConfig.SpeedRoulette,_staticData.GameConfig.RouletteScore, _soundService);
            RouletteView = rouletteView;
            return RouletteView;
        }

        public ChipsView CreateChipsView()
        {
            GameObject roulettePrefab = _assetProvider.GetChipsPrefab();
            Vector3 position = _staticData.LocationData.PositionChips.ToVector3();
            ChipsView chipsView = Object.Instantiate(roulettePrefab, position, Quaternion.identity).GetComponent<ChipsView>();
            chipsView.Initialize(_staticData.GameConfig.ChipsScore, _soundService);
            ChipsView = chipsView;
            return ChipsView;
        }
        
        public DiceView CreateDiceView()
        {
            GameObject diceViewPrefab = _assetProvider.GetCardDiceViewPrefab();
            Vector3 position = _staticData.LocationData.PositionDice.ToVector3();
            DiceView diceView = Object.Instantiate(diceViewPrefab, position, Quaternion.identity).GetComponent<DiceView>();
            diceView.Initialize(_staticData.GameConfig.DiceData, _staticData.GameConfig.TimeDiceRotate, _soundService);
            DiceView = diceView;
            return DiceView;
        }

        public SlotMachineView CreateSlotMachineView()
        {
            GameObject slotMachine = _assetProvider.GetSlotMachineViewPrefab();
            Vector3 position = _staticData.LocationData.PositionSlotMachine.ToVector3();
            SlotMachineView slotMachineView = Object.Instantiate(slotMachine, position, Quaternion.identity).GetComponent<SlotMachineView>();
            slotMachineView.Initialize(_staticData.GameConfig.SlotSymbolAngles,
                _staticData.GameConfig.TimeSlotDrumRotate, _staticData.GameConfig.ScoreSlotMachine, _soundService);
            SlotMachineView = slotMachineView;
            return SlotMachineView;
        }
        
        public void CreateFlippers()
        {
            LeftFlipper = CreateFlipper(_assetProvider.GetLeftFlipperPrefab(),
                _staticData.LocationData.PositionLeftFlipper.ToVector3());
            RightFlipper = CreateFlipper(_assetProvider.GetRightFlipperPrefab(),
                _staticData.LocationData.PositionRightFlipper.ToVector3());
        }
        
        public TouchFlipperHandler CreateTouchControl()
        {
            TouchFlipperHandler touchFlipperHandler = new TouchFlipperHandler(LeftFlipper, RightFlipper);
            TouchFlipperHandler = touchFlipperHandler;
            return TouchFlipperHandler;
        }
        
        private Flipper CreateFlipper(GameObject prefab, Vector3 position) =>
            Object.Instantiate(prefab, position, Quaternion.identity).GetComponent<Flipper>();
    }
}