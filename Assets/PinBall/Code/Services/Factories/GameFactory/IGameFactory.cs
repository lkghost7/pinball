using PinBall.Code.Core.GamePlay.CardRoulette;
using PinBall.Code.Core.GamePlay.Chips;
using PinBall.Code.Core.GamePlay.Dice;
using PinBall.Code.Core.GamePlay.Flippers;
using PinBall.Code.Core.GamePlay.Roulette;
using PinBall.Code.Core.GamePlay.Slots;
using PinBall.Code.Core.GamePlay.Spring;
using PinBall.Code.Infrastructure.ServiceContainer;
using UnityEngine;

namespace PinBall.Code.Services.Factories.GameFactory
{
    public interface IGameFactory : IService
    {
        string ActiveScene { get; set; }
        public SpringView SpringView { get; }
        public GameObject Ball { get; }
        public Flipper LeftFlipper { get; }
        public Flipper RightFlipper { get; }
        public CardRouletteView CardRouletteView { get; }
        public RouletteView RouletteView { get; }
        public ChipsView ChipsView { get; }
        public DiceView DiceView { get; }
        public SlotMachineView SlotMachineView { get; }
        public TouchFlipperHandler TouchFlipperHandler { get; }

        SpringView CreateSpring();
        GameObject CreateBall();
        CardRouletteView CreateCardRoulette();
        RouletteView CreateRouletteView();
        ChipsView CreateChipsView();
        DiceView CreateDiceView();
        void CreateFlippers();
        SlotMachineView CreateSlotMachineView();
        TouchFlipperHandler CreateTouchControl();
    } 
}