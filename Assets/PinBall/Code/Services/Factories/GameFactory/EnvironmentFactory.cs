using PinBall.Code.Core.GamePlay.Environment;
using PinBall.Code.Core.GamePlay.Score;
using PinBall.Code.Core.GamePlay.Trampoline;
using PinBall.Code.Core.GamePlay.Triggers;
using PinBall.Code.Infrastructure.StateMachine;
using PinBall.Code.Services.Dispose;
using PinBall.Code.Services.Factories.UIFactory;
using PinBall.Code.Services.PersistentProgress;
using PinBall.Code.Services.Providers.AssetProvider;
using PinBall.Code.Services.SaveLoad;
using PinBall.Code.Services.Sound;
using PinBall.Code.Services.StaticData;
using UnityEngine;

namespace PinBall.Code.Services.Factories.GameFactory
{
    public class EnvironmentFactory : IEnvironmentFactory
    {
        public ScoreCalculator ScoreCalculator { get; private set; }
        public BallBlocker BallBlocker { get; private set; }
        public EndGameTrigger EndGameTrigger { get; private set; }
        public AspectRatioChanger AspectRatioChanger { get; private set; }

        private readonly IDisposeService _disposeService;
        private readonly IAssetProvider _assetProvider;
        private readonly IStaticData _staticData;
        private readonly IPersistentProgress _playerProgress;
        private readonly ISoundService _soundService;
        private readonly ISaveLoad _saveLoad;
        private readonly IUIFactory _uiFactory;
        private readonly IGameStateMachine _stateMachine;

        public EnvironmentFactory(IDisposeService disposeService, IAssetProvider assetProvider,
            IStaticData staticData, IPersistentProgress playerProgress,
            ISoundService soundService, ISaveLoad saveLoad, IUIFactory uiFactory, IGameStateMachine stateMachine)
        {
            _soundService = soundService;
            _playerProgress = playerProgress;
            _staticData = staticData;
            _assetProvider = assetProvider;
            _disposeService = disposeService;
            _saveLoad = saveLoad;
            _uiFactory = uiFactory;
            _stateMachine = stateMachine;
        }
        
        public BallBlocker CreateBallBlocker()
        {
            GameObject ballBlockerPrefab = _assetProvider.GetBallBlockerPrefab();
            Vector3 position = _staticData.LocationData.PositionRoundTrigger.ToVector3();
            BallBlocker = Object.Instantiate(ballBlockerPrefab, position, Quaternion.identity).GetComponent<BallBlocker>();
            return BallBlocker;
        }

        public EndGameTrigger CreateEndGameTrigger()
        {
            GameObject triggerPrefab = _assetProvider.GetEndGameTriggerPrefab();
            Vector3 position = _staticData.LocationData.PositionEndGameTrigger.ToVector3();
            EndGameTrigger = Object.Instantiate(triggerPrefab, position, Quaternion.identity).GetComponent<EndGameTrigger>();
            return EndGameTrigger;
        }

        public void CreateEnvironment()
        {
            GameObject environmentPrefab = _assetProvider.GetEnvironmentPrefab();
            Vector3 position = _staticData.LocationData.PositionEnvironment.ToVector3();
            EnvironmentView environmentView = Object.Instantiate(environmentPrefab, position, Quaternion.identity).GetComponent<EnvironmentView>();
            environmentView.Initialize();
        }
        
        public ScoreCalculator CreateScoreCalculator()
        {
            ScoreCalculator scoreCalculator = new ScoreCalculator(_playerProgress, _saveLoad, _uiFactory);
            ScoreCalculator = scoreCalculator;
            return scoreCalculator;
        }

        public AspectRatioChanger CreateAspectRatioChanger()
        {
            AspectRatioChanger ratioChanger = new AspectRatioChanger(_staticData.GameConfig.devicesData);
            AspectRatioChanger = ratioChanger;
            return ratioChanger;
        }

        public void CreateTrampolines()
        {
            CreateTrampolines(_assetProvider.GetTrampolineLeftPrefab(),
                _staticData.LocationData.PositionTrumplineLeft.ToVector3());
            CreateTrampolines(_assetProvider.GetTrampolineRightPrefab(),
                _staticData.LocationData.PositionTrumplineRight.ToVector3());
        }

        private TrampolineView CreateTrampolines(GameObject prefab, Vector3 position)
        {
            TrampolineView trampolineView = Object.Instantiate(prefab, position, Quaternion.identity).GetComponent<TrampolineView>();
            trampolineView.Initialize(_soundService);
            return trampolineView;
        }
    }
}