using PinBall.Code.Core.GamePlay.Environment;
using PinBall.Code.Core.GamePlay.Score;
using PinBall.Code.Core.GamePlay.Triggers;
using PinBall.Code.Infrastructure.ServiceContainer;

namespace PinBall.Code.Services.Factories.GameFactory
{
    public interface IEnvironmentFactory : IService
    {
        public ScoreCalculator ScoreCalculator { get; }
        public BallBlocker BallBlocker { get; }
        public EndGameTrigger EndGameTrigger { get; }
        public AspectRatioChanger AspectRatioChanger { get; }
        
        EndGameTrigger CreateEndGameTrigger();
        BallBlocker CreateBallBlocker();
        void CreateTrampolines();
        void CreateEnvironment();
        ScoreCalculator CreateScoreCalculator();
        AspectRatioChanger CreateAspectRatioChanger();
    } 
}