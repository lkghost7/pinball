using PinBall.Code.Data.StaticData;
using PinBall.Code.Data.StaticData.Locations;
using PinBall.Code.Data.StaticData.Sounds;
using PinBall.Code.Services.Providers.StaticDataProvider;

namespace PinBall.Code.Services.StaticData
{
    public class StaticData : IStaticData
    {
        public GameConfig GameConfig { get; private set; }
        public LocationData LocationData { get; private set; }
        public SoundData SoundData { get; private set; }
        
        private readonly IStaticDataProvider _staticDataProvider;

        public StaticData(IStaticDataProvider staticDataProvider) =>
            _staticDataProvider = staticDataProvider;

        public void LoadStaticData()
        {
            GameConfig = _staticDataProvider.GetGameConfig();
            LocationData = _staticDataProvider.GetLocationData();
            SoundData = _staticDataProvider.GetSoundData();
        }
    }
}