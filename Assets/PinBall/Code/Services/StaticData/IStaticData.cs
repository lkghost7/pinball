using PinBall.Code.Data.StaticData;
using PinBall.Code.Data.StaticData.Locations;
using PinBall.Code.Data.StaticData.Sounds;
using PinBall.Code.Infrastructure.ServiceContainer;

namespace PinBall.Code.Services.StaticData
{
    public interface IStaticData : IService
    {
        GameConfig GameConfig { get; }
        LocationData LocationData { get; }
        SoundData SoundData { get; }
        void LoadStaticData();
    }
}