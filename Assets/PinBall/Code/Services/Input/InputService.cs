using System;
using PinBall.Code.Data.Enums;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PinBall.Code.Services.Input
{
    public class InputService : IInputService
    {
        public event Action<Vector2, TouchStatus> OnTouchPressed;
        public event Action<TouchStatus> OnTouchDepressed;

        private readonly GameInput _gameInput;
        public InputService()
        {
            _gameInput = new GameInput();

            _gameInput.TouchControl.FirstTouchPress.started += FirstTouchPressed;
            _gameInput.TouchControl.FirstTouchPress.canceled += FirstTouchDepressed;
            _gameInput.TouchControl.ScondaryTouchPress.started += SecondaryTouchPressed;
            _gameInput.TouchControl.ScondaryTouchPress.canceled += SecondaryTouchDepressed;
        }

        public void EnableInput() => _gameInput.Enable();
        public void DisableInput() => _gameInput.Disable();

        private void FirstTouchPressed(InputAction.CallbackContext context) => OnTouchPressed?.Invoke(_gameInput.TouchControl.FirstTouchPosition.ReadValue<Vector2>(),TouchStatus.First);
        private void FirstTouchDepressed(InputAction.CallbackContext context) => OnTouchDepressed?.Invoke(TouchStatus.First);
        private void SecondaryTouchPressed(InputAction.CallbackContext context) => OnTouchPressed?.Invoke(_gameInput.TouchControl.SecondaryTouchPosition.ReadValue<Vector2>(),TouchStatus.Second);
        private void SecondaryTouchDepressed(InputAction.CallbackContext context) => OnTouchDepressed?.Invoke(TouchStatus.Second);

        ~InputService()
        {
            _gameInput.TouchControl.FirstTouchPress.started -= FirstTouchPressed;
            _gameInput.TouchControl.FirstTouchPress.canceled -= FirstTouchDepressed;
            _gameInput.TouchControl.ScondaryTouchPress.started -= SecondaryTouchPressed;
            _gameInput.TouchControl.ScondaryTouchPress.canceled -= SecondaryTouchDepressed;
        }
    }
}