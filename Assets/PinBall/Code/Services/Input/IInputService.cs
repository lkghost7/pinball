using System;
using PinBall.Code.Data.Enums;
using PinBall.Code.Infrastructure.ServiceContainer;
using UnityEngine;

namespace PinBall.Code.Services.Input
{
    public interface IInputService : IService
    {
        event Action<Vector2, TouchStatus> OnTouchPressed;
        event Action<TouchStatus> OnTouchDepressed;
        
        void EnableInput();
        void DisableInput();
    }
}