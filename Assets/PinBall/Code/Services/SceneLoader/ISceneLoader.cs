using System;
using PinBall.Code.Infrastructure.ServiceContainer;

namespace PinBall.Code.Services.SceneLoader
{
    public interface ISceneLoader : IService
    {
        void LoadScene(string sceneName, Action onLoaded = null);
    }
}