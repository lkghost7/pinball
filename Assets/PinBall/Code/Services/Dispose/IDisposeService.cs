using System;
using PinBall.Code.Infrastructure.ServiceContainer;

namespace PinBall.Code.Services.Dispose
{
    public interface IDisposeService : IService
    {
        void RegisterDisposable(IDisposable disposable, string scene);
        void DisposeAll(string scene);
    }
}