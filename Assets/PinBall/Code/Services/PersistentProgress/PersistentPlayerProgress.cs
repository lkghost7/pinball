using PinBall.Code.Data.Progress;

namespace PinBall.Code.Services.PersistentProgress
{
    public class PersistentPlayerProgress : IPersistentProgress
    {
        public PlayerProgress Progress { get; set; }
    }
}