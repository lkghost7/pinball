using PinBall.Code.Data.Progress;
using PinBall.Code.Infrastructure.ServiceContainer;

namespace PinBall.Code.Services.PersistentProgress
{
    public interface IPersistentProgress : IService
    {
        PlayerProgress Progress { get; set; }
    }
}