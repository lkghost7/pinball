using PinBall.Code.Data.StaticData;
using PinBall.Code.Data.StaticData.Locations;
using PinBall.Code.Data.StaticData.Sounds;
using PinBall.Code.Infrastructure.ServiceContainer;

namespace PinBall.Code.Services.Providers.StaticDataProvider
{
    public interface IStaticDataProvider : IService
    {
        GameConfig GetGameConfig();
        LocationData GetLocationData();
        SoundData GetSoundData();
    }
}