using PinBall.Code.Data.StaticData;
using PinBall.Code.Data.StaticData.Locations;
using PinBall.Code.Data.StaticData.Sounds;
using UnityEngine;

namespace PinBall.Code.Services.Providers.StaticDataProvider
{
    public class StaticDataProvider : IStaticDataProvider
    {
        private const string GameConfigPath = "StaticData/Game Config";
        private const string LocationDataPath = "StaticData/Location Data";
        private const string SoundDataPath = "StaticData/Sound Data";

        public GameConfig GetGameConfig() => Resources.Load<GameConfig>(GameConfigPath);
        public LocationData GetLocationData() => Resources.Load<LocationData>(LocationDataPath);
        public SoundData GetSoundData() => Resources.Load<SoundData>(SoundDataPath);
    }
}