using PinBall.Code.Infrastructure.ServiceContainer;
using UnityEngine;

namespace PinBall.Code.Services.Providers.AssetProvider
{
    public interface IAssetProvider : IService
    {
        GameObject GetRootGameplayUIPrefab();
        GameObject GetMainMenuPrefab();
        GameObject GetTopPanelView();
        GameObject GetExitPanelView();
        GameObject GetGameOverPanelView();
        GameObject GetSettingsPanelPrefab();
        
        GameObject GetSpringPrefab();
        GameObject GetBallPrefab();
        GameObject GetBallBlockerPrefab();
        GameObject GetEndGameTriggerPrefab();
        GameObject GetLeftFlipperPrefab();
        GameObject GetRightFlipperPrefab(); 
        GameObject GetCardRoulettePrefab();
        GameObject GetCardDiceViewPrefab();
        GameObject GetSlotMachineViewPrefab();
        GameObject GetTrampolineLeftPrefab();
        GameObject GetTrampolineRightPrefab();
        GameObject GetRoulettePrefab();
        GameObject GetChipsPrefab();
        GameObject GetEnvironmentPrefab();
    }
}