using UnityEngine;

namespace PinBall.Code.Services.Providers.AssetProvider
{
    public class AssetProvider : IAssetProvider
    {
        private const string RootGameplayUIPrefab = "Prefabs/UI/RootGameplayUI";
        private const string MainMenuPrefabPath = "Prefabs/UI/MainMenu";
        private const string SettingsPanelPrefabPath = "Prefabs/UI/SettingsPanel";
        private const string TopPanelPrefabPath = "Prefabs/UI/TopPanel";
        private const string ExitPanelViewPrefabPath = "Prefabs/UI/ExitPanel";
        private const string GameOverPanelViewPrefabPath = "Prefabs/UI/GameOverPanel";
        
        private const string SpringPrefabPath = "Prefabs/Game/Spring";
        private const string BallPrefabPath = "Prefabs/Game/Ball";
        private const string BallBlockerPrefabPath = "Prefabs/Game/BallBlocker";
        private const string EndGameTriggerPrefabPath = "Prefabs/Game/EndGameTrigger";
        
        private const string LeftFlipperPrefabPath = "Prefabs/Game/LeftFlipper";
        private const string RightFlipperPrefabPath = "Prefabs/Game/RightFlipper";
        private const string GetCardRoulettePrefabPath = "Prefabs/Game/CardRoulette";
        private const string GetDiceViewPrefabPath = "Prefabs/Environment/Dice";
        private const string GetSlotMachineViewPrefabPath = "Prefabs/SlotMachine/SlotMachine";
        
        private const string GetTrampolineLeftPrefabPath = "Prefabs/Environment/TrumplineLeft";
        private const string GetTrampolineRightPrefabPath = "Prefabs/Environment/TrumplineRight";
        
        private const string GetRoulettePrefabPath = "Prefabs/Environment/Roulette";
        private const string GetChipsPrefabPath = "Prefabs/Environment/Chips";
        private const string GetEnvironmentPrefabPath = "Prefabs/Environment/Environment";

        public GameObject GetMainMenuPrefab() => Resources.Load<GameObject>(MainMenuPrefabPath);
        public GameObject GetTopPanelView() => Resources.Load<GameObject>(TopPanelPrefabPath);
        public GameObject GetExitPanelView() => Resources.Load<GameObject>(ExitPanelViewPrefabPath);
        public GameObject GetGameOverPanelView() => Resources.Load<GameObject>(GameOverPanelViewPrefabPath);
        public GameObject GetSettingsPanelPrefab() => Resources.Load<GameObject>(SettingsPanelPrefabPath);
        public GameObject GetRootGameplayUIPrefab() => Resources.Load<GameObject>(RootGameplayUIPrefab);
        
        public GameObject GetSpringPrefab() => Resources.Load<GameObject>(SpringPrefabPath);
        public GameObject GetBallPrefab() => Resources.Load<GameObject>(BallPrefabPath);
        public GameObject GetBallBlockerPrefab() => Resources.Load<GameObject>(BallBlockerPrefabPath);
        public GameObject GetEndGameTriggerPrefab() => Resources.Load<GameObject>(EndGameTriggerPrefabPath);
        
        public GameObject GetLeftFlipperPrefab() => Resources.Load<GameObject>(LeftFlipperPrefabPath);
        public GameObject GetRightFlipperPrefab() => Resources.Load<GameObject>(RightFlipperPrefabPath);
        public GameObject GetCardRoulettePrefab() => Resources.Load<GameObject>(GetCardRoulettePrefabPath);
        public GameObject GetCardDiceViewPrefab() => Resources.Load<GameObject>(GetDiceViewPrefabPath);
        public GameObject GetSlotMachineViewPrefab() => Resources.Load<GameObject>(GetSlotMachineViewPrefabPath);
        public GameObject GetTrampolineLeftPrefab() => Resources.Load<GameObject>(GetTrampolineLeftPrefabPath);
        public GameObject GetTrampolineRightPrefab() => Resources.Load<GameObject>(GetTrampolineRightPrefabPath);
        public GameObject GetRoulettePrefab() => Resources.Load<GameObject>(GetRoulettePrefabPath);
        public GameObject GetChipsPrefab() => Resources.Load<GameObject>(GetChipsPrefabPath);
        public GameObject GetEnvironmentPrefab() => Resources.Load<GameObject>(GetEnvironmentPrefabPath);
    }
}